from structgenerator import generate_measurement_part,\
                            generate_structural_part, create_model_description
from paramgenerator import generate_parameters
from datagenerator import generate_data

mpart = generate_measurement_part(4)
spart, tm = generate_structural_part(mpart, 10, num_cycles=0)
params_mpart, params_spart = generate_parameters(mpart, spart)
data = generate_data(mpart, spart, params_mpart, params_spart, 200, tm)
data.to_csv('data.txt', sep=',')
model = create_model_description(mpart, spart)
with open('modTest.txt', 'w') as f:
    f.write(model)
print(model)
