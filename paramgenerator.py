from numpy.random import exponential, normal
from functools import partial


DEFAULT_EXP = partial(exponential, 1)


def generate_parameters(mpart: dict, spart: dict,
                        mpart_generator=DEFAULT_EXP,
                        spart_generator=DEFAULT_EXP,
                        mpart_fix_value=1.0):
    '''
Generates random parameters for the proposed model.
Keyword arguments:
    mpart           -- A measurement part.
    spart           -- A structural part.
    mpart_generator -- A function f() that is used to randomly generate
                       parameters for measurement part.
    spart_generator -- A function f() that is used to randomly generate
                       parameters for structural part.
    mpart_fix_value -- A value to fix with firsts indicators for each latent
                       variable.
Returns:
    Two dictionaries with parameters for spart and mpart in the form
    {'SomeVariable': [(y1, 1.0), (y2, 5.5)]}
    '''
    return generate_parameters_part(mpart, mpart_generator, mpart_fix_value),\
           generate_parameters_part(spart, spart_generator)


def generate_parameters_part(part: dict, generator, fix_first=None):
    d = dict()
    for v, variables in part.items():
        d[v] = list()
        variables = sorted(variables)
        it = iter(variables)
        if fix_first is not None:
            var = next(it)
            d[v].append((var, fix_first))
        for var in it:
            d[v].append((var, generator()))
    return d
